<?
    session_start();

    if (
        !isset($_POST['employedbefore']) ||
        !isset($_POST['teacherexperience']) ||
        !isset($_POST['eadexperience']) ||
        !isset($_POST['contentmanagerexperience']) ||
        !isset($_POST['otherexperiences'])
    ) {
        var_dump($_POST);
        header('location: index.php');
        die();
    }

    $_SESSION['employedbefore'] = $_POST['employedbefore'];
    $_SESSION['teacherexperience'] = $_POST['teacherexperience'];
    $_SESSION['eadexperience'] = $_POST['eadexperience'];
    $_SESSION['contentmanagerexperience'] = $_POST['contentmanagerexperience'];
    $_SESSION['otherexperiences'] = $_POST['otherexperiences'];
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title = "Formulário de Inscrição"; 
        $css = ['css/default.css', 'css/fifthFormHandle.css', 'css/logo.css', 'css/font-awesome.css', 'css/errorMsg.css'];
        require_once(__DIR__.'/model/header.php'); ?>
    <script>
        errorMsgs = [];

        let casa = '10';

        let message = {
            fileCheckMsg: '',
            fileSizeMsg: '',
            fileExtensionMsg: ''
        };

        function errorMsg(msg) {
            let outerDiv = document.createElement('div');
            outerDiv.classList.add('msg-background');
            outerDiv.innerHTML = '<div class="msg-wrapper"><span>'+msg+'</span><br><button class="b-button" onclick="deleteMsg(this)">Fechar</button></div>';
            errorMsgs.push(outerDiv);
            document.body.appendChild(outerDiv);
        }

        function deleteMsg(element) {
            element.parentNode.parentNode.remove();
        }

        function checkDoubleRGOnFile() {
            let element = document.getElementById('rgCheckBox');
            if(element && element.checked) {
                return true;
            }

            return false;
        }

        function checkFileExtension(input) {
            if(!input || !input.files || input.files && input.files.length == 0) {
                return true;
            }

            // Verifica a extensão:
            let filename = input.files[0].name;
            if(filename.split('.').pop().toLowerCase() != 'pdf') {
                message.fileExtensionMsg = 'O formato de arquivo do campo ' + input.previousElementSibling.previousElementSibling.innerText + ' está incorreto. É permitido apenas arquivos em PDF.';
                return false;
            }

            return true;
        }

        function checkFileSize(input) {
            if(!input || !input.files || input.files && input.files.length == 0) {
                return true;
            }

            // Verifica o tamanho:
            if(input.files[0].size > 2097152) {
                let filename = input.previousElementSibling.previousElementSibling.innerText;
                message.fileSizeMsg = 'O tamanho dos arquivos a seguir é maior que o limite máximo permitido(2MB):<br>' + filename;
                return false;
            }

            return true;
        }

        function validateFileForm(input) {
            if(
                !input ||
                !input.files ||
                input.files && input.files.length == 0
            ) {
                message.fileSizeMsg = 'O campo <b>' + input.previousElementSibling.previousElementSibling.innerText + '</b> não foi preenchido.';
                return false;
            }

            return true;
        }
        
        function onSubmit(form) {
            if(
                !form ||
                !validateFileForm(form.lattesfile) ||
                !validateFileForm(form.rgfilefront) ||
                !checkDoubleRGOnFile() && !validateFileForm(form.rgfileverse) ||
                !validateFileForm(form.cpffile) ||
                !validateFileForm(form.electorfile) ||
                !validateFileForm(form.residencefile) ||
                !validateFileForm(form.graduationfilefront) ||
                !validateFileForm(form.graduationfileverse)
            ) {
                errorMsg(message.fileSizeMsg);
                return false;
            }

            if(
                !checkFileExtension(form.lattesfile) ||
                !checkFileExtension(form.rgfilefront) ||
                !checkFileExtension(form.rgfileverse) ||
                !checkFileExtension(form.cpffile) ||
                !checkFileExtension(form.electorfile) ||
                !checkFileExtension(form.residencefile) ||
                !checkFileExtension(form.graduationfilefront) ||
                !checkFileExtension(form.graduationfileverse) ||
                !checkFileExtension(form.posgraduationfilefront) ||
                !checkFileExtension(form.posgraduationfileverse) ||
                !checkFileExtension(form.otherfiles1) ||
                !checkFileExtension(form.otherfiles2) ||
                !checkFileExtension(form.otherfiles3)
            ) {
                errorMsg(message.fileExtensionMsg);
                return false;
            }

            if(
                !checkFileSize(form.lattesfile) ||
                !checkFileSize(form.rgfilefront) ||
                !checkFileSize(form.rgfileverse) ||
                !checkFileSize(form.cpffile) ||
                !checkFileSize(form.electorfile) ||
                !checkFileSize(form.residencefile) ||
                !checkFileSize(form.graduationfilefront) ||
                !checkFileSize(form.graduationfileverse) ||
                !checkFileSize(form.posgraduationfilefront) ||
                !checkFileSize(form.posgraduationfileverse) ||
                !checkFileSize(form.otherfiles1) ||
                !checkFileSize(form.otherfiles2) ||
                !checkFileSize(form.otherfiles3)
            ) {
                errorMsg(message.fileSizeMsg);
                return false;
            }

            return false;
        }

        function displayFileName(element) {
            if(!element.files) {
                return;
            }

            if(element.files.length > 0) {
                let span = element.nextElementSibling;
                span.innerText = element.files[0].name;

                let button = element.previousElementSibling;
                button.innerText = 'Mudar arquivo...';
            } else {
                let span = element.nextElementSibling;
                span.innerText = '';

                let button = element.previousElementSibling;
                button.innerText = 'Anexar arquivo...';
            }
        }

        function rgcheckbox(element) {
            let form = element.form;
            if(!form) {
                return;
            }

            let rgfileverse = form.rgfileverse;
            let rgfilefront = form.rgfilefront;

            if(!rgfileverse || !rgfilefront) {
                return;
            }

            let span = rgfilefront.previousElementSibling.previousElementSibling.lastChild;

            if(element.checked) {
                rgfileverse.parentNode.hidden = true;
                span.innerText = '';
            } else {
                rgfileverse.parentNode.hidden = false;
                span.innerText = '(frente)';
            }
        }
    </script>
</head>
<body class="content-wrapper">
    <? require_once('top_info.php'); ?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="images/logo_v2.png" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <div class="center"><span><strong>Envio de Documentos</strong></span></div>
        <br>
        <div class="right"><span class="warning">OBS: Apenas arquivos em formato PDF, máximo de 2MB por arquivo.</span></div>
        <br>
        <form enctype="multipart/form-data" action="finalFormHandle.php" method="POST" onsubmit="return onSubmit(this)">
            <label class="formInput required">
                <span>Currículo Lattes</span>:
                <span tabindex="1" class="b-button">Anexar arquivo...</span>
                <input type="file" name="lattesfile" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput spacing required">
                <span>RG<span>(frente)</span></span>:
                <span tabindex="2" class="b-button">Anexar arquivo...</span>
                <input type="file" name="rgfilefront" onchange="displayFileName(this)">
                <span></span>
            </label><br>

            <label class="formInput spacing required">
                <span>RG<span>(verso)</span></span>:
                <span tabindex="3" class="b-button">Anexar arquivo...</span>
                <input type="file" name="rgfileverse" onchange="displayFileName(this)">
                <span></span>
            </label><br>

            <label><input tabindex="4" id="rgCheckBox" type="checkbox" name="onergfile" onclick="rgcheckbox(this)"> Frente e verso na mesma folha. </label><br>
            <br>

            <label class="formInput required">
                <span>CPF</span>:
                <span tabindex="5" class="b-button">Anexar arquivo...</span>
                <input type="file" name="cpffile" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput required">
                <span>Título de eleitor</span>:
                <span tabindex="6" class="b-button">Anexar arquivo...</span>
                <input type="file" name="electorfile" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput required">
                <span>Comprovante de residência</span>: 
                <span tabindex="7" class="b-button">Anexar arquivo...</span>
                <input type="file" name="residencefile" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput required">
                <span>Diploma de Curso de Graduação Frente</span>:
                <span tabindex="8" class="b-button">Anexar arquivo...</span>
                <input type="file" name="graduationfilefront" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput required">
                <span>Diploma de Curso de Graduação Verso</span>:
                <span tabindex="9" class="b-button">Anexar arquivo...</span>
                <input type="file" name="graduationfileverse" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput">
                <span>Diploma de Curso de Pós-Graduação Frente (se houver)</span>:
                <span tabindex="10" class="b-button">Anexar arquivo...</span>
                <input type="file" name="posgraduationfilefront" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput">
                <span>Diploma de Curso de Pós-Graduação Verso (se houver)</span>: 
                <span tabindex="11" class="b-button">Anexar arquivo...</span>
                <input type="file" name="posgraduationfileverse" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput">
                <span>Outros arquivos (1)</span>: 
                <span tabindex="12" class="b-button">Anexar arquivo...</span>
                <input type="file" name="otherfiles1" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput">
                <span>Outros arquivos (2)</span>: 
                <span tabindex="13" class="b-button">Anexar arquivo...</span>
                <input type="file" name="otherfiles2" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <label class="formInput">
                <span>Outros arquivos (3)</span>: 
                <span tabindex="14" class="b-button">Anexar arquivo...</span>
                <input type="file" name="otherfiles3" onchange="displayFileName(this)">
                <span></span>
            </label><br>
            <br>

            <div class="center"><button class="b-button" type="submit">Enviar</button></div>
        </form>
    </div>
</body>
</html>