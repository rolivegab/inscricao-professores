<?
    session_start();

    if (
        !isset($_POST['school_level']) ||
        !isset($_POST['graduation_for'])
    ) {
        header('location: index.php');
        die();
    }

    $_SESSION['school_level'] = $_POST['school_level'];
    $_SESSION['graduation_for'] = $_POST['graduation_for'];
    if(isset($_POST['pos_graduation_for'])) {
        $_SESSION['pos_graduation_for'] = $_POST['pos_graduation_for'];
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title = "Formulário de Inscrição";
        $css = ['css/default.css', 'css/fourthFormHandle.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php'); ?>
    <script>
        function onSubmit(form) {
            if(
                !form ||
                !form.employedbefore ||
                !form.teacherexperience ||
                !form.eadexperience ||
                !form.contentmanagerexperience ||
                !form.otherexperiences
            ) {
                return false;
            }

            return true;
        }
    </script>
</head>
<body class="content-wrapper">
    <? require_once('top_info.php'); ?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="images/logo_v2.png" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <div class="center"><strong>Experiência profissional</strong></div>
        <br><br>
        <form action="fifthFormHandle.php" method="POST" onsubmit="return onSubmit(this)">
            <span class="question required">É ou já foi colaborador da Unifacex?</span><br>
            <br>
            <label> 
                <input type="radio" name="employedbefore" value="yes" required>
                Sim
            </label><br>
            <label>
                <input type="radio" name="employedbefore" value="no" required>
                Não
            </label><br>
            <br><br>

            <label class="question required">
                Detalhamento da experiência como docência:<br>
                <textarea rows="5" name="teacherexperience" maxlength="500" required></textarea>
            </label><br>
            <br>

            <label class="question required">
                Detalhamento da experiência EAD:<br>
                <textarea rows="5" name="eadexperience" maxlength="500" required></textarea>
            </label><br>
            <br>

            <label class="question required">
                Detalhamento da experiência na produção de conteúdo:<br>
                <textarea rows="5" name="contentmanagerexperience" maxlength="500" required></textarea>
            </label><br>
            <br>

            <label class="question required">
                Outras experiências relevantes:<br>
                <textarea rows="5" name="otherexperiences" maxlength="500" required></textarea>
            </label><br>
            <br>

            <div class="center"><button class="b-button" type="submit">Próximo</button></div>
        </form>
    </div>
</body>
</html>