<?
    require_once(__DIR__.'/CDB.php');
    use admin\CDB;
    session_start();

    if(!isset($_SESSION['logged'])) {
        include_once(__DIR__.'/notLogged.php');
        die();
    }

    $db = new CDB();
    $submissions = $db->submissions();
?>