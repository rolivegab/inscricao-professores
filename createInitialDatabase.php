<?

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once('ConnDB.php');

    global $COURSES;
    global $CFG;

    if(file_exists($CFG->file)) {
        echo 'O arquivo de banco de dados já existe.<br>';

        if(isset($_GET['convert']) && $_GET['convert'] = 'true') {
            echo 'Iniciando processo de conversão.<br>';

            $db = new ConnDB();
            $db->activeForeignKeys();

            $cpfdb = new ConnDB('cpf.sqlite3');

            $originals = $cpfdb->executeSQL("SELECT * FROM submissions")->fetchAll();

            foreach ($originals as $original) {
                $query = "INSERT INTO submission VALUES (
                    '$original->cpf', 
                    '$original->role', 
                    '$original->name', 
                    '$original->rg', 
                    '$original->address', 
                    '$original->number', 
                    '$original->cep', 
                    '$original->city', 
                    '$original->tel', 
                    '$original->email', 
                    '$original->school_level', 
                    '$original->graduation_for', 
                    '$original->pos_graduation_for', 
                    '$original->employedbefore', 
                    '$original->teacherexperience', 
                    '$original->eadexperience', 
                    '$original->contentmanagerexperience', 
                    '$original->otherexperiences', 
                    '$original->lattesfile', 
                    '$original->rgfilefront', 
                    '$original->rgfileverse', 
                    '$original->cpffile', 
                    '$original->electorfile', 
                    '$original->residencefile', 
                    '$original->graduationfilefront', 
                    '$original->graduationfileverse', 
                    '$original->posgraduationfilefront', 
                    '$original->posgraduationfileverse', 
                    '$original->otherfiles1', 
                    '$original->otherfiles2', 
                    '$original->otherfiles3')";

                $sql = $db->executeSQL($query);

                // Relacionando cursos:
                $area1 = $db->executeSQL("SELECT * FROM area WHERE area.name = :name", ['name' => $COURSES[$original->area1]['name'] ])->fetch()->id;
                $course1 = $db->executeSQL("SELECT * FROM course WHERE course.name = :name", ['name' => $COURSES[$original->area1]['childs'][$original->course1]])->fetch()->id;
                $db->executeSQL("INSERT INTO submission_course VALUES ($course1, '$original->cpf')");
                if($original->area2 != "NULL") {
                    $area2 = $db->executeSQL("SELECT * FROM area WHERE area.name = :name", ['name' => $COURSES[$original->area2]['name'] ])->fetch()->id;
                    $course2 = $db->executeSQL("SELECT * FROM course WHERE course.name = :name", ['name' => $COURSES[$original->area2]['childs'][$original->course2]])->fetch()->id;
                    $db->executeSQL("INSERT INTO submission_course VALUES ($course2, '$original->cpf')");
                }
                if($original->area3 != "NULL") {
                    $area3 = $db->executeSQL("SELECT * FROM area WHERE area.name = :name", ['name' => $COURSES[$original->area3]['name'] ])->fetch()->id;
                    $course3 = $db->executeSQL("SELECT * FROM course WHERE course.name = :name", ['name' => $COURSES[$original->area3]['childs'][$original->course3]])->fetch()->id;
                    $db->executeSQL("INSERT INTO submission_course VALUES ($course3, '$original->cpf')");
                }
            }
        }
        die();
    }

    $db = new ConnDB();
    $db->activeForeignKeys();

    // Initializing database:
    $sql = file_get_contents('sql/rules.sql');
    $db->executeMultipleSQL("$sql");

    foreach($COURSES as $area) {
        // Inserindo cada area:
        $result = $db->executeSQL("INSERT INTO area VALUES (null, :area)", ['area' => $area['name']]);
        $lastareaid = $db->lastInsertID();
        foreach($area['childs'] as $course) {
            // Inserindo cada curso:
            $db->executeSQL("INSERT INTO course VALUES(null, :course)", ['course' => $course]);
            $lastcourseid = $db->lastInsertID();

            // Relacionando curso inserido com a area inserida:
            $db->executeSQL("INSERT INTO area_course VALUES(:areaid, :courseid)", ['areaid' => $lastareaid, 'courseid' => $lastcourseid]);
        }
    }