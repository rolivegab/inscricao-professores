<? namespace course;

    require_once(__DIR__.'/CDB.php');

    if(!isset($_GET['cpf'])) {
        die();
    }

    $cpf = $_GET['cpf'];

    $db = new CDB();

    // POST -> ADD COURSE
    if(isset($_POST['course'])) {
        $courseid = $_POST['course'];
        $db->add_course_to_submission($courseid, $cpf);
    }

    // POST -> DELETE COURSE
    if(isset($_POST['deletecourse'])) {
        $courseid = $_POST['deletecourse'];
        $db->delete_course_from_submission($courseid, $cpf);
    }

    $courses_from_submission = $db->courses_from_submission(['id', 'name'], $cpf);
    $courses = $db->courses();
    ?>
    <a href="course.php">Voltar</a>
    <table>
        <thead>
            <th>courseid</th>
            <th>name</th>
        </thead>
        <tbody><?
            foreach($courses_from_submission as $row) {
            ?>
                <tr>
                    <td><?=$row->id?></td>
                    <td><?=$row->name?></td>
                    <td><form action="" method="post" style="margin: 0"><input type="hidden" name="deletecourse" value="<?=$row->id?>"><button>apagar</button></form></td>
                </tr>
            <?
            }
        ?></tbody>
    </table>
    <br><br>

    <form action="" method="post">
        <label>
            <span>Adicionar: </span><br>
            <select name="course"><?
                foreach($courses as $course) {
                ?>
                    <option value="<?=$course->id?>"><?=$course->name?></option>
                <?
                }?>
            </select><br>
            <br>
            <button type="submission">Adicionar</button>
        </label>
    </form>