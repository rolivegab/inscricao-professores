<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <? 
            $title="Formulário de Inscrição";
            $captcha=true;
            $css=['css/default.css', 'css/logo.css', 'css/font-awesome.css'];
            require_once( __DIR__.'/model/header.php' );
            header('HTTP/1.0 403 Forbidden');
        ?>
    </head>
<body class="content-wrapper">
    <div class="content">
        <div class="center"><span class="question">Você não deveria ter permissão para acessar essa página =(</span></div><br>
    </div>
</body>
</html>