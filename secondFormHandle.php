<?
    session_start();
    
    require_once(__DIR__.'/lib.php');
    require_once(__DIR__.'/ConnDB.php');
    require_once(__DIR__.'/config.php');
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title = "Formulário de Inscrição"; 
        $css = ['css/default.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php'); 
    ?>
    <script>
        function onSubmit(form) {
            if(
                !form ||
                !form.name ||
                !form.rg ||
                !form.address ||
                !form.number ||
                !form.cep ||
                !form.city ||
                !form.tel ||
                !form.email
            ) {
                return false;
            }

            return true;
        }
    </script>
</head>

</html>