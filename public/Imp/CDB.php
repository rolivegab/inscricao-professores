<?
    namespace course {
        require_once( realpath(__DIR__.'/../Core/ConnDB.php'));
        require_once(__DIR__.'/lib.php');
        use ConnDB;
        use PDO;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
            }

            function courses_from_submission($columns, $cpf) {
                sanitize($columns, ['id', 'name']);
                $sql = 'SELECT '.param($columns).' FROM submission_course LEFT JOIN course ON course.id = submission_course.courseid WHERE cpf=:cpf';
                $params = [
                    'cpf' => $cpf
                ];
                $stmt = $this->prepare($sql, $params);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function add_course_to_submission($courseid, $cpf) {
                $sql = "INSERT INTO submission_course VALUES (:courseid, :cpf)";
                $stmt = $this->prepare($sql, ['cpf' => $cpf, 'courseid' => $courseid]);
                $stmt->execute();
                return $this->lastInsertID();
            }

            function delete_course_from_submission($courseid, $cpf) {
                $sql = "DELETE FROM submission_course WHERE cpf=:cpf AND courseid=:courseid";
                $stmt = $this->prepare($sql, ['cpf' => $cpf, 'courseid' => $courseid]);
                return $stmt->execute();
            }

            function submission($columns) {
                if(!sanitize($columns, ['cpf', 'name', 'email'])) {
                    return;
                }
                $sql = 'SELECT '.param($columns).' FROM submission ORDER BY name';
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function null_courses_from_submission($columns) {
                if(!sanitize($columns, ['cpf', 'name', 'email'])) {
                    return;
                }
                $sql = 'SELECT '.param(array_map(function($param) { return 'submission.'.$param; }, $columns)).' FROM submission LEFT JOIN submission_course ON submission_course.cpf = submission.cpf WHERE submission_course.courseid IS NULL';
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function courses() {
                $sql = 'SELECT * FROM course ORDER BY name';
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }
        };
    }

    namespace formHandle {
        require_once( realpath(__DIR__.'/../Core/ConnDB.php'));
        require_once(__DIR__.'/lib.php');
        use ConnDB;
        use PDO;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
            }

            function get_courses_from_area($areaid) {
                $sql = "SELECT course.id, course.name FROM area_course LEFT JOIN course ON course.id = area_course.courseid WHERE areaid = :areaid";
                $stmt = $this->prepare($sql, ['areaid' => $areaid]);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function checkCPF($cpf) {
                $sql = "SELECT 0 FROM submission WHERE cpf = :cpf";
                $stmt = $this->prepare($sql, ['cpf' => $cpf]);
                $stmt->execute();
                return $stmt->fetch();
            }

            function area() {
                $sql = "SELECT * FROM area";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }
        };
    }

    namespace finalSubmission {
        require_once( realpath(__DIR__.'/../Core/ConnDB.php'));
        require_once(__DIR__.'/lib.php');
        require_once( realpath(__DIR__.'/../Core/Log.php'));
        use ConnDB;
        use PDO;
        use Log;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
                $this->log = new Log('finalSubmission-CDB.php');
            }

            public function saveSubmission($params) {
                $sql = "INSERT INTO submission VALUES (
                    :cpf,
                    :role,
                    :name,
                    :rg,
                    :address,
                    :number,
                    :cep,
                    :city,
                    :tel,
                    :email,
                    :school_level,
                    :graduation_for,
                    :pos_graduation_for,
                    :employedbefore,
                    :teacherexperience,
                    :eadexperience,
                    :contentmanagerexperience,
                    :otherexperiences,
                    :lattesfile,
                    :rgfilefront,
                    :rgfileverse,
                    :cpffile,
                    :electorfile,
                    :residencefile,
                    :graduationfilefront,
                    :graduationfileverse,
                    :posgraduationfilefront,
                    :posgraduationfileverse,
                    :otherfiles1,
                    :otherfiles2,
                    :otherfiles3
                );";

                $stmt = $this->prepare($sql, $params);
                $stmt->execute();
                return $this->lastInsertID();
            }
        };
    }

    namespace admin {
        require_once( realpath(__DIR__.'/../Core/ConnDB.php'));
        require_once(__DIR__.'/lib.php');
        use ConnDB;
        use PDO;

        class CDB extends ConnDB {
            function __construct($file = NULL) {
                parent::__construct($file);
            }

            function areas() {
                $sql = "SELECT area.id, area.name FROM area";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function courses_from_area($areaid) {
                $sql = 
                "SELECT 
                    course.id, 
                    course.name 
                FROM course 
                LEFT JOIN area_course ON area_course.courseid = course.id WHERE area_course.areaid = :areaid";
                $stmt = $this->prepare($sql, ['areaid' => $areaid]);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function submissions_from_course($courseid) {
                $sql = 
                "SELECT
                    submission.cpf,
                    submission.name,
                    submission.email
                FROM submission
                LEFT JOIN submission_course ON submission_course.cpf = submission.cpf WHERE submission_course.courseid = :courseid";
                $stmt = $this->prepare($sql, ['courseid' => $courseid]);
                $stmt->execute();
                return $stmt->fetchAll();
            }

            function submission_with_cpf($cpf) {
                $sql = "SELECT * FROM submission WHERE cpf = :cpf";
                $stmt = $this->prepare($sql, ['cpf' => $cpf]);
                $stmt->execute();
                return $stmt->fetch();
            }

            function submissions() {
                $sql = "SELECT cpf, name FROM submission";
                $stmt = $this->prepare($sql);
                $stmt->execute();
                return $stmt->fetchAll();
            }
        }
    }
