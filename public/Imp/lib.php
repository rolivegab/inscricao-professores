<?

function validaCPF($cpf) {
    // Verifica se foi informado todos os digitos corretamente
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }
    // Faz o calculo para validar o CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return false;
        }
    }
    return true;
}

function sanitize($parameters, $originals) {
    $originals = array_flip($originals);
    foreach($parameters as $p) {
        if(!key_exists($p, $originals)) {
            return false;
        }
    }

    return true;
}

function param($columns) {
    return implode(", ", $columns);
}