<?php
    class SM
    {
        public static function StartSession()
        {
            // $better_token = md5(uniqid(rand(), true));
            $status = session_status();
            if ($status == PHP_SESSION_NONE) {
                session_start();
            }
        }

        // public static function getID() {
        //     return session_id();
        // }

        // public static function setID(string $string): void {
        //     session_id($string);
        // }
        
        public static function isPOST($var) {
            return isset($_POST[$var]);
        }
        
        public static function isGET($var) {
            return isset($_GET[$var]);
        }
        
        public static function isSESSION($var) {
            static::StartSession();

            return isset($_SESSION[$var]);
        }

        public static function getGET($var) {
            return $_GET[$var];
        }
        
        
        public static function getPOST($var) {
            return $_POST[$var];
        }

        public static function getSESSION($var) {
            SM::StartSession();
            return $_SESSION[$var];
        }

        public static function setSESSION($var, $valor) {
            SM::StartSession();
            $_SESSION[$var] = $valor;
        }

        public static function closeSESSION() {
            SM::StartSession();
            session_unset();
            session_destroy();
        }
        
        public static function unsetSESSION($var) {
            SM::StartSession();
            unset($_SESSION[$var]);
        }

        public static function parseJSON() {
            static::$json = json_decode(file_get_contents('php://input', true));

            if(!empty(static::$json)) {
                static::$isjson = true;
            }
        }

        public static function isJSON($var) {
            if(!static::$isjson) {
                return false;
            }

            return isset(static::$json->$var);
        }

        public static function getJSON($var) {
            $aux = static::$json->$var;
            return $aux;
        }

        private static $json;
        private static $isjson = false;
    }