<?
    require_once( realpath(__DIR__.'/../../config.php'));

    class ConnDB
    {
        public function __construct($file = NULL) {
            global $CFG;

            // Permite a utilizaÃ§Ã£o das credenciais nesta classe.
            if(!$file) {
                $this->file = $CFG->file;
            } else {
                $this->file = $file;
            }
            $this->dblib = $CFG->dblib;
            $this->Connect();
            $this->activeForeignKeys();
        }

        public function Connect() {
            try {
                $this->dbconn = new PDO("$this->dblib:$this->file");
            } catch(Exception $e) {
                $e->getMessage();
                return false;
            }

            $this->dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbconn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            return true;
        }

        protected function activeForeignKeys() {
            $this->prepare('PRAGMA foreign_keys = ON;')->execute();
        }

        protected function prepare($sql, $params = NULL) {   
            $stmt = $this->dbconn->prepare($sql);
            if($params) {
                foreach($params as $key=>$param) {
                    if(is_int($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_INT);
                    } else if (is_string($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_STR);
                    }
                }
            }

            return $stmt;
        }

        protected function lastInsertID() {
            return $this->dbconn->lastInsertID();
        }

        protected function getDBConn() {
            return $this->dbconn;
        }

        private $dbconn;

        private $dbname;
        private $dbuser;
        private $dbpassword;
        private $dbhost;
        private $dblib;
        private $isInTransaction;
    }
