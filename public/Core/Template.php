<?
    require_once(realpath(__DIR__.'/../../config.php'));
    require_once(realpath(__DIR__.'/SM.php'));

    class T extends SM {
        function __construct($language, $title) {
            $this->language = $language;
            $this->title = $title;
            $this->resourceStyles = [];
            $this->localStyles = [];
            $this->localScripts = [];
            global $CFG;
            static::$webroot = $CFG->webroot;
            $this->p = new stdClass();
        }

        function setLocalStyles($styles) {
            foreach($styles as $style) {
                $this->localStyles[] = $style;
            }
        }

        function setLocalScripts($scripts) {
            foreach($scripts as $script) {
                $this->localScripts[] = $script;
            }
        }

        function render() {
            $this->loadHTML();
        }

        function setResourceStyles($styles) {
            foreach($styles as $style) {
                $this->resourceStyles[] = $style;
            }
        }

        function loadHeader() {
            ?>
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                    <title><?=$this->loadTitle()?></title>
                    <?=$this->loadStyles()?>
                    <?=$this->loadScripts()?>
                </head>
            <?
        }

        function loadTitle() {
            return $this->title;
        }

        function loadStyles() {
            foreach($this->resourceStyles as $style) {
                ?><link rel="stylesheet" href="<?=$this->webURL('/css/'.$style.'.css')?>"><?
            }

            foreach($this->localStyles as $style) {
                ?><link rel="stylesheet" href="<?='css/'.$style.'.css'?>"><?
            }
        }

        function loadScripts() {
            foreach($this->localScripts as $script) {
                ?><script src="<?='script/'.$script.'.js'?>"></script><?
            }
        }

        static function webURL($url) {
            return static::$webroot.$url;
        }

        function loadHTML() {
            ?>
                <!DOCTYPE html>
                <html lang=<?=$this->loadLanguage()?>>
                <?=$this->loadHeader()?>
                </html>
                <body>
                    <?=$this->loadBody()?>
                </body>
            <?
        }

        function loadLanguage() {
            return $this->language;
        }

        function loadBody() {
            return $this->body;
        }

        static function resource($file) {
            return static::webURL($file);
        }

        function loadModel($dir) {
            $p = $this->p;
            ob_start();
            require($dir.'/model.php');
            $a = ob_get_contents();
            ob_end_clean();
            $this->body = $a;
        }

        static function page($page) {
            ob_start();
            require(realpath(__DIR__.'/../page/'.$page.'/index.php'));
            $a = ob_get_contents();
            ob_end_clean();
            return $a;
        }

        static function getPageURL($page) {
            return static::webURL('/page/'.$page);
        }

        function addPage($param, $page) {
            $this->add($param, static::page($page));
        }

        function add($param, $value) {
            $this->p->$param = $value;
        }

        function addResource($param, $value) {
            $this->add($param, T::resource($value));
        }

        private $language;
        private $title;
        private $resourceStyles;
        private $localStyles;
        private $p;
        private $localdir;
        static private $webroot;
    }