<?
    require_once( realpath(__DIR__.'/../../config.php'));

    class Log {
        function __construct($name) {
            global $CFG;
            $this->group = $CFG->apachegroup;
            $this->name = $name;
            $this->logdir = __DIR__.'/log/';
            $this->year = date('y');
            $this->month = date('m');
            $this->day = date('d');
            
            $this->dirpath = $this->logdir.$this->year.'/'.$this->month.'/'.$this->day.'/';
            $this->filepath = $this->dirpath.$this->name;

            if(!$this->checkDir()) {
                $this->createLog();
            }
        }

        function checkPermissions() {
            return is_writable($this->logdir);
        }

        function checkDir() {
            return is_dir($this->dirpath);
        }

        function createLog() {
            $yeardir = $this->logdir.$this->year;
            $monthdir = $yeardir.'/'.$this->month;
            $daydir = $monthdir.'/'.$this->day;

            return (
                $this->createDir($yeardir) &&
                $this->createDir($monthdir) &&
                $this->createDir($daydir)
            );
        }

        private function header() {
			$this->putL('====== '.date('d-m-Y H:i:s').' ======');
        }
        
        public function close() {
			$this->put('#'.PHP_EOL);
		}

        function put($text) {
			if(file_exists($this->filepath)) {
				if(!$this->headerAlreadyPrinted) {
					$this->headerAlreadyPrinted = true;
					$this->header();
				}

				file_put_contents($this->filepath, $text, FILE_APPEND);
			} else {
				file_put_contents($this->filepath, $text, FILE_APPEND);
				chgrp($this->filepath, $this->group);
				chmod($this->filepath, 0770);
			}
        }
        
        function putL($text) {
            $this->put($text.PHP_EOL);
        }

        function createDir($dir) {
            return (
                is_dir($dir) ||
                mkdir($dir) &&
                chmod($dir, 0755) && 
                chgrp($dir, $this->group)
            );
        }

        private $headerAlreadyPrinted;

        private $group;
        private $name;
        private $logdir;

        private $year;
        private $month;
        private $day;

        private $path;
    }