<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));
    require_once( realpath(__DIR__.'/../../Imp/CDB.php'));
    require_once( realpath(__DIR__.'/../../Imp/lib.php'));
    use formHandle\CDB;

    if(T::isPOST('cpf')) {
        $cpf = T::getPOST('cpf');
        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        if(!validaCPF($cpf)) {
            header('Location: ../parte-1/?cpf=false');
        } else {
            $db = new CDB();
            $sql = $db->checkCPF($cpf);
            if($sql) {
                header('location: ../cpfexiste/?cpf='.$cpf);
                die();
            } else {
                header('location: ../parte-2');
            }
        }

        die();
    }
    $dir =__DIR__;
    $T = new T('pt-br', 'Inscrição professores');

    $T->setResourceStyles(['default', 'logo']);
    $T->setLocalStyles(['input']);
    $T->setLocalScripts(['cpfCheck']);
    $T->addResource('logo', '/image/logo.png');
    $T->addPage('topinfo', 'topinfo');

    $T->loadModel(__DIR__);
    $T->render();