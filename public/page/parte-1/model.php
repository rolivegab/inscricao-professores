<div class="content-wrapper">
    <?=$p->topinfo?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="<?=$p->logo?>" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <form action="" method="POST">
            <label class="required-right">
                <input contenteditable="true" type="text" name="cpf" id="form_cpf" placeholder="CPF" maxlength="14" onkeypress="cpfOnKeyPress(event)" onpaste="cpfOnPaste(event)" required>
                <span id="cpferror" class="error"></span>
            </label><br>
            <label class="b-text">
                <input type="checkbox" required>
                Declaro que li e estou de acordo com todos os critérios de seleção e diretrizes estabelecidos no <a href="edital.pdf" download="Edital - Cadastro de Reserva - Professor Conteudista Pós-Graduação.pdf">edital</a> 01/2018 NEAD-UNIFACEX.
            </label>
            <div class="center"><button class="b-button" type="submit">Próximo</button></div>
            <!--<div class="g-recaptcha" data-sitekey="6Le0ykcUAAAAAASc9weA3-jMjuKUqOjNU1ob8VLl"></div>-->
        </form>
    </div>
</div>