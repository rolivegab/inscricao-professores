// Caso tenha parâmetros GET:
let cpfcheck;
try {
    cpfcheck = document.URL.split('?')[1].split('&')[0].split('=');
} catch(err) {
    cpfcheck = false;
} finally {
    window.onload = function() {
        if(cpfcheck[0] === 'cpf' && cpfcheck[1] === 'false') {
            document.getElementById('cpferror').innerText = 'CPF Inválido';
        }
    }
}

function cpfOnKeyPress(e) {
    let input = e.target;
    let value = input.value;
    let length = value.length;

    if(length === 3 || length === 7) {
        value += '.';
    }
    if(length === 11) {
        value += '-';
    }

    input.value = value;
}

function cpfOnPaste(e) {
    let input = e.target;
    let value = e.clipboardData.getData('Text');

    if(value.length > 3) {
        value = value.slice(0,3) + '.' + value.slice(3);
    }
    if(value.length > 7) {
        value = value.slice(0,7) + '.' + value.slice(7);
    }
    if(value.length > 11) {
        value = value.slice(0,11) + '-' + value.slice(11);
    }

    input.value = value;
}