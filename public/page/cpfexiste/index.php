<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));
    require_once( realpath(__DIR__.'/../../Core/SM.php'));

    $T = new T('pt-br', 'Inscrição professores');

    $T->setResourceStyles(['default', 'logo']);
    // $T->setLocalStyles(['input']);
    // $T->setLocalScripts(['cpfCheck']);
    $T->addResource('logo', '/image/logo.png');
    $T->addPage('topinfo', 'topinfo');
    $T->add('cpf', SM::getGET('cpf'));

    $T->loadModel(__DIR__);
    $T->render();