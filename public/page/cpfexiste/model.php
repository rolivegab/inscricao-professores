<div class="content-wrapper">
    <div class="content">
        <?=$p->topinfo?>
        <div class="center"><img class="b-image" src="<?=$p->logo?>" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <div class="center"><span>Já foi realizada uma inscrição com CPF igual a <b><?=$p->cpf?></b>.<br><br>
        Se você acredita que isso foi um erro, favor entrar em contato com a nossa equipe.<br>Mais informações no topo da página. </span></div><br>
        <br><br>
        <form action="../index">
            <button class="b-button">Voltar</button>
        </form>
    </div>
</div>