<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));
    require_once( realpath(__DIR__.'/../../Core/SM.php'));
    require_once( realpath(__DIR__.'/../../Imp/CDB.php'));
    use admin\CDB;

    if(!SM::isSESSION('logged')) {
        header('Location: ../notLogged');
        die();
    }

    $db = new CDB();

    $T = new T('pt-br', 'Inscrição professores');
    $T->setResourceStyles(['default', 'logo']);
    $T->setLocalStyles(['viewSubmission']);
    $T->setLocalScripts(['redirectToViewUser']);

    // Parameters:
    $p = new stdClass();
    $T->addResource('logo', '/image/logo.png');
    $T->add('areas', $db->areas());
    $T->add('courses_from_area', function($areaid) use ($db) {
        return $db->courses_from_area($areaid);
    });
    $T->add('submissions_from_course', function($courseid) use ($db) {
        return $db->submissions_from_course($courseid);
    });
    $T->add('submissions', $db->submissions());

    $T->loadModel(__DIR__, $p);
    $T->render();