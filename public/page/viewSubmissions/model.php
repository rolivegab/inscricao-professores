<div class="content-wrapper">
    <div class="content">
        <div class="center"><span class="question">ADMINISTRAÇÃO</span></div><br>
        <br>
        <strong><span> Total de submissões: <?=count($p->submissions)?> </span></strong>
        <?foreach($p->areas as $area): ?>

            <div data-areaid="<?=$area->id?>">
                <span><?=$area->name?></span>
                <?foreach(call_user_func($p->courses_from_area, $area->id) as $course):?>

                    <div data-courseid="<?=$course->id?>">
                        <span><?=$course->name?></span>
                        <table>
                            <tbody>
                                <?foreach(call_user_func($p->submissions_from_course, $course->id) as $s):?>
                                    <tr onclick="viewUser(this)" data-submissioncpf=<?=$s->cpf?>>
                                        <td><?=$s->cpf?></a></td>
                                        <td><?=$s->name?></td>
                                        <td><?=$s->email?></td>
                                    </tr>
                                <?endforeach?>
                            </tbody>
                        </table>
                    </div>

                <?endforeach?>
            </div>

        <?endforeach?>
        <table>
            <thead>
                <th>CPF</th>
                <th>Nome</th>
            </thead>
            <?foreach($p->submissions as $s):?>
                <tr onclick="viewUser(this)" data-cpf=<?=$s->cpf?>>
                    <td><?=$s->cpf?></td>
                    <td><?=$s->name?></td>
                </tr>
            <?endforeach?>
        </table>        
    </div>
</div>