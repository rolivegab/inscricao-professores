<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));
    require_once( realpath(__DIR__.'/../../Imp/lib.php'));
    require_once( realpath(__DIR__.'/../../Core/SM.php'));
    require_once( realpath(__DIR__.'/../../Imp/CDB.php'));
    use admin\CDB;
    use course\CDB as courseCDB;

    $T = new T('pt-br', 'Inscrição professores');

    if(!SM::isSESSION('logged') || !SM::isGET('cpf')) {
        header('Location: ../notLogged/');
        die();
    }

    $db = new CDB();
    $cdb = new courseCDB();

    $submission = $db->submission_with_cpf($_GET['cpf']);
    if(!$submission) {
        die();
    }
    $courses = $cdb->courses_from_submission(['id', 'name'], $_GET['cpf']);

    function filename($file) {
        return pathinfo($file, PATHINFO_FILENAME).'.pdf';
    }

    $T->add('submission', $submission);
    $T->add('courses', $courses);
    $T->add('lattesfile', filename($submission->lattesfile));
    $T->add('rgfilefront', filename($submission->rgfilefront));
    $T->add('rgfileverse', filename($submission->rgfileverse));
    $T->add('cpffile', filename($submission->cpffile));
    $T->add('electorfile', filename($submission->electorfile));
    $T->add('residencefile', filename($submission->residencefile));
    $T->add('graduationfilefront', filename($submission->graduationfilefront));
    $T->add('graduationfileverse', filename($submission->graduationfileverse));
    $T->add('posgraduationfilefront', filename($submission->posgraduationfilefront));
    $T->add('posgraduationfileverse', filename($submission->posgraduationfileverse));
    $T->add('otherfiles1', filename($submission->otherfiles1));
    $T->add('otherfiles2', filename($submission->otherfiles2));
    $T->add('otherfiles3', filename($submission->otherfiles3));

    $T->setResourceStyles(['default', 'logo']);
    $T->setLocalStyles(['property']);

    $T->addResource('logo', '/image/logo.png');
    $T->addPage('topinfo', 'topinfo');

    $T->loadModel(__DIR__);
    $T->render();