<body class="content-wrapper">
    <div class="content">
        <div class="center"><span class="question">Visualização de usuário</span></div>
        <div class="center" id="checkboxes"></div>
        <span class="property">CPF:</span><span><?= $p->submission->cpf ?></span>
        <span class="property">Cargo:</span><span id="role"><?= $p->submission->role ?></span>

        <div class="block" data-type="Cursos">
            <?foreach ($p->courses as $key=>$course):?>
                <span class="property">Curso <?=$key?>:</span><span><?=$course->name?></span>
            <?endforeach?>
        </div>

        <div class="block" data-type="Dados Cadastrais">    
            <span class="property">Nome:</span><span><?= $p->submission->name ?></span>
            <span class="property">RG:</span><span><?= $p->submission->rg ?></span>
            <span class="property">Endereço:</span><span><?= $p->submission->address ?></span>
            <span class="property">Número:</span><span><?= $p->submission->number ?></span>
            <span class="property">CEP:</span><span><?= $p->submission->cep ?></span>
            <span class="property">Cidade:</span><span><?= $p->submission->city ?></span>
            <span class="property">Telefone:</span><span><?= $p->submission->tel ?></span>
        </div>
        <div class="block" data-type="Formação Acadêmica">
            <span class="property">Maior nível:</span><span><?= $p->submission->school_level ?></span>
            <span class="property">Graduação em:</span><span><?= $p->submission->graduation_for ?></span>
            <span class="property">Pós-Graduação em (maior nível, se houver):</span><span><?= $p->submission->pos_graduation_for ?></span>
        </div>
        <div class="block" data-type="Experiência Profissional">
            <span class="property">Já foi colaborador da UNIFACEX?</span><span><?= $p->submission->employedbefore ?></span>
            <span class="property">Detalhamento da experiência como docência:</span><span><?= $p->submission->teacherexperience ?></span>
            <span class="property">Detalhamento da experiência EAD:</span><span><?= $p->submission->eadexperience ?></span>
            <span class="property">Detalhamento da experiência na produção de conteúdo:</span><span><?= $p->submission->contentmanagerexperience ?></span>
            <span class="property">Outras experiências relevantes:</span><span><?= $p->submission->otherexperiences ?></span>
        </div>
        <div class="block" data-type="Arquivos">
            <span class="property">Arquivo Lattes:</span><a href=<?='../viewFile?file='.$p->lattesfile?> target="_blank">Baixar</a>
            <span class="property">RG Frente:</span><a href=<?='../viewFile?file='.$p->rgfilefront?> target="_blank">Baixar</a>
            <span class="property">RG Verso:</span><a href=<?='../viewFile?file='.$p->rgfileverse?> target="_blank">Baixar</a>
            <span class="property">CPF:</span><a href=<?='../viewFile?file='.$p->cpffile?> target="_blank">Baixar</a>
            <span class="property">Título de eleitor:</span><a href=<?='../viewFile?file='.$p->electorfile?> target="_blank">Baixar</a>
            <span class="property">Comprovante de residência:</span><a href=<?='../viewFile?file='.$p->residencefile?> target="_blank">Baixar</a>
            <span class="property">Graduação Frente:</span><a href=<?='../viewFile?file='.$p->graduationfilefront?> target="_blank">Baixar</a>
            <span class="property">Graduação Verso:</span><a href=<?='../viewFile?file='.$p->graduationfileverse?> target="_blank">Baixar</a>
            <span class="property">Pós-Graduação Frente:</span><a href=<?='../viewFile?file='.$p->posgraduationfilefront?> target="_blank">Baixar</a>
            <span class="property">Pós-Graduação Verso:</span><a href=<?='../viewFile?file='.$p->posgraduationfileverse?> target="_blank">Baixar</a>
            <span class="property">Outros arquivos 1:</span><a href=<?='../viewFile?file='.$p->otherfiles1?> target="_blank">Baixar</a>
            <span class="property">Outros arquivos 2:</span><a href=<?='../viewFile?file='.$p->otherfiles2?> target="_blank">Baixar</a>
            <span class="property">Outros arquivos 3:</span><a href=<?='../viewFile?file='.$p->otherfiles3?> target="_blank">Baixar</a>
        </div>
    </div>
</body>