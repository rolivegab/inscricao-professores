<div class="content-wrapper">
    <div class="content">
        <form action="" method="POST">
            <div class="center"><img class="b-image" src="<?=$p->logo?>" alt="Logo da Sala Virtual da UNIFACEX"></div>
            <div class="center"><span class="question">Login</span></div><br>
            <br>
            <label class="required-right">
                <input type="text" name="username" id="username" placeholder="Usuário" required>
            </label><br>
            <label class="required-right">
                <input type="password" name="password" id="password" placeholder="Senha" required>
            </label><br>
            <br><br>
            <button class="b-button" type="submit">Autenticar</button>
        </form>
    </div>
</div>