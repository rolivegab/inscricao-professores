<?
    require_once( realpath(__DIR__.'/../../Core/SM.php'));

    if(
        SM::isPOST('username') &&
        SM::isPOST('password')
    ) {
        if(SM::getPOST('username') == 'admin' && SM::getPOST('password') == 'Facex@123') {
            SM::setSESSION('logged', true);
            header('location: ../viewSubmissions');
            die();
        }
    }

    require_once( realpath(__DIR__.'/../../Core/Template.php'));

    $T = new T('pt-br', 'Inscrição professores');
    $T->setResourceStyles(['default', 'logo']);

    // Parameters:
    $p = new stdClass();
    $T->addResource('logo', '/image/logo.png');

    $T->loadModel(__DIR__, $p);
    $T->render();