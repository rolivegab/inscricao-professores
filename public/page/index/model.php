<div class="content-wrapper">
    <div class="content">
        <div class="center"><img class="b-image" src="<?=$p->logo?>" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <p class="b-text">Bem-vindo ao sistema de inscrição do processo seletivo para professor conteudista e professor revisor de disciplinas EAD – Unifacex. Nos próximos passos serão demandados os documentos e informações previstos no edital 01/2018 do NEaD - Unifacex.</p>
        <p class="b-text">Acesse o edital <a href="<?=$p->filepath?>" download="Edital - Cadastro de Reserva - Professor Conteudista Pós-Graduação.pdf">aqui</a>.</p>
        <p class="b-text"><b>Edital prorrogado até o dia 18 de Março de 2018.</b></p>
        <p class="b-text">Se você possuir alguma dúvida entre em contato via email: ead@unifacex.com.br</p>
        <div class="center"><a href="<?=$p->nextPage?>"><button class="b-button">Iniciar inscrição</button></a></div>
    </div>
</div>