<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));
    require_once( realpath(__DIR__.'/../../../config.php'));

    global $CFG;
    if($CFG->maintenancemode === true) {
        header('Location: ../maintenance-mode/');
        die();
    }

    $T = new T(__DIR__, 'pt-br', 'Inscrição professores');
    $T->setResourceStyles(['default', 'logo']);

    // Parameters:
    $T->addResource('logo', '/image/logo.png');
    $T->addResource('filepath', '/file/edital.pdf');
    $T->addResource('nextPage', '/page/parte-1');

    // Model:
    $T->loadModel(__DIR__);
    $T->render();
?>