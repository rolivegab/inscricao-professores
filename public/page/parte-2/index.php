<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));
    require_once( realpath(__DIR__.'/../../Imp/lib.php'));
    require_once( realpath(__DIR__.'/../../Core/SM.php'));
    require_once( realpath(__DIR__.'/../../Imp/CDB.php'));
    use formHandle\CDB;

    // Checking for POST:
    if(
        SM::isPOST('role') &&
        SM::isPOST('area1') &&
        (!SM::isPOST('area2') || SM::isPOST('course2')) &&
        (!SM::isPOST('area3') || SM::isPOST('course3'))
    ) {
        SM::setSESSION('role', SM::getPOST('role'));
        SM::setSESSION('area1', SM::getPOST('area1'));
        SM::setSESSION('course1', SM::getPOST('course1'));
        if(SM::isPOST('area2')) {
            SM::setSESSION('area2', SM::getPOST('course2'));
            SM::setSESSION('course2', SM::getPOST('course2'));
        }
        if(SM::isPOST('area3')) {
            SM::setSESSION('area3', SM::getPOST('course3'));
            SM::setSESSION('course3', SM::getPOST('course3'));
        }

        header('Location: ../parte-3/');
        die();
    }

    // Checking for AJAX:
    SM::parseJSON();
    if(SM::isJSON('command'))
    {
        $command = SM::getJSON('command');
        if($command == 'area') {
            $db = new CDB();
            echo json_encode($db->area());
            die();
        } else if($command == 'course') {
            $db = new CDB();
            echo json_encode($db->get_courses_from_area(SM::getJSON('areaid')));
            die();
        }
    }

    $T = new T('pt-br', 'Inscrição professores');

    $T->setLocalStyles(['parte-2']);
    $T->setResourceStyles(['default', 'logo']);
    $T->setLocalScripts(['script']);

    $T->addResource('logo', '/image/logo.png');
    $T->addPage('topinfo', 'topinfo');

    $T->loadModel(__DIR__);
    $T->render();