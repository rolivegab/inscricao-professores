<div class="content-wrapper">
    <?=$p->topinfo?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="<?=$p->logo?>" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <form action="" method="POST" onsubmit="return onSubmit(this)">
            <span class="question required">Quero me candidatar ao cargo de: </span><br>
            <br>
            <label>
                <input type="radio" name="role" value="teacher" required> Professor conteudista
            </label><br>
            <label>
                <input type="radio" name="role" value="reviewer" required> Revisor de conteúdo
            </label><br>
            <label>
                <input type="radio" name="role" value="both" required> Ambos
            </label><br>
            <br>

            <div>
                <label class="question required">Selecionar curso 1:<br>
                    <select class="b-select" id="area1" name="area1" onchange="areaOnChange(this, 1)">
                    </select>
                </label><br>
                <br>

                <label class="question required" hidden>Selecionar disciplina 1:<br>
                    <select class="b-select" id="course1" name="course1">
                    </select>
                </label>
            </div>
            <div id="divcourse2" hidden>
                <br>
                <hr>
                <br>
                <label class="question required">Selecionar curso 2:<br>
                    <select class="b-select" id="area2" name="area2" onchange="areaOnChange(this, 2)">
                    </select>
                </label><br>
                <br>

                <label class="question required" hidden>Selecionar disciplina 2:<br>
                    <select class="b-select" id="course2" name="course2">
                    </select>
                </label>
            </div>
            <div id="divcourse3" hidden>
                <br>
                <hr>
                <br>
                <label class="question required">Selecionar curso 3:<br>
                    <select class="b-select" id="area3" name="area3" onchange="areaOnChange(this, 3)">
                    </select>
                </label><br>
                <br>

                <label class="question required" hidden>Selecionar disciplina 3:<br>
                    <select class="b-select" id="course3" name="course3">
                    </select>
                </label>
            </div>
            <br>
            <button id="areaincrement" type="button" onclick="areaStateIncrement(this); return false"> Adicionar mais um curso </button><br>
            <button id="areadecrement" type="button" onclick="areaStateDecrement(this); return false" hidden> Remover último curso </button><br>
            <br>

            <div class="center"><button class="b-button" type="submit"> Próximo </button></div>
        </form>
    </div>
</div>
