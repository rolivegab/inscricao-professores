let areaState = 1;

function request(header, callback) {
    let xmlhttp = new XMLHttpRequest();
    let file = 'index.php';

    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
            if (callback) {
                callback(xmlhttp.responseText);
            }
        } else {
            // Fazer barra de carregamento aqui.
        }
    };

    xmlhttp.onerror = function(e) {
        console.log(e.message);
        return false;
    };

    xmlhttp.open('POST', file, true);
    xmlhttp.setRequestHeader('Content-type', 'application/json');
    xmlhttp.withCredentials = true;
    xmlhttp.send(JSON.stringify(header));
}

function areaStateIncrement(element) {
    if (areaState === 1) {
        document.getElementById('divcourse2').hidden = false;
        document.getElementById('areadecrement').hidden = false;
        firstCategoryLoad(2);
    } else if (areaState === 2) {
        document.getElementById('divcourse3').hidden = false;
        element.hidden = true;
        firstCategoryLoad(3);
    } else {
        return;
    }

    areaState++;
}

function areaStateDecrement(element) {
    if (areaState === 2) {
        document.getElementById('divcourse2').hidden = true;
        document.getElementById('area2').innerHTML = '';
        document.getElementById('divcourse2').innerHTML = '';
        element.hidden = true;
    } else if (areaState === 3) {
        document.getElementById('divcourse3').hidden = true;
        document.getElementById('areaincrement').hidden = false;
        document.getElementById('area3').innerHTML = '';
        document.getElementById('divcourse3').innerHTML = '';
    } else {
        return;
    }

    areaState--;
}

window.onload = function(index) {
    firstCategoryLoad(1);
}

function areaOnChange(element, index) {
    let areaid = element.value;
    let select = document.getElementById('course' + index);
    let label = select.parentNode;
    if (select) {
        select.innerHTML = '<option value="default">Selecionar</option>';
    } else {
        return;
    }
    if(areaid == 'default') {
        label.hidden = true;
        return;
    }
    let header = {
        command: 'course',
        areaid: areaid
    };
    request(header, function(responseText) {
        let courses = JSON.parse(responseText);
        for(let i = 0; i < courses.length; ++i) {
            let option = document.createElement('option');
            option.value = courses[i].id;
            option.innerText = courses[i].name;
            select.appendChild(option);
            label.hidden = false;
        }
    });
}

function onSubmit(form) {
    if(
        areaState == 1 && (form.area1.value === 'default' || form.course1.value === 'default') ||
        areaState == 2 && (form.area2.value === 'default' || form.course2.value === 'default') ||
        areaState == 3 && (form.area3.value === 'default' || form.course3.value === 'default')
    ) {
        return false;
    }

    if(
        areaState == 2 && (
            form.course1.value === form.course2.value
        ) ||
        areaState == 3 && (
            form.course1.value === form.course2.value || 
            form.course1.value === form.course3.value ||
            form.course2.value === form.course3.value
        )
    ) {
        return false;
    }

    return true;
}

function firstCategoryLoad(id) {
    let select = document.getElementById('area' + id);
    if (select) {
        let option = document.createElement('option');
        select.innerHTML = '<option value="default">Selecionar</option>';

        let header = {
            command: 'area'
        }
        request(header, function(courses) {
            courses = JSON.parse(courses);
            for(let i = 0; i < courses.length; ++i) {
                let option = document.createElement('option');
                option.value = courses[i].id;
                option.innerText = courses[i].name;
                select.appendChild(option);
            }
        });
    }
}