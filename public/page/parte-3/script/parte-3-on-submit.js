function onSubmit(form) {
    if(
        !form ||
        !form.name ||
        !form.rg ||
        !form.address ||
        !form.number ||
        !form.cep ||
        !form.city ||
        !form.tel ||
        !form.email
    ) {
        return false;
    }

    return true;
}

function cepOnChange(e) {
    if(e.target.value.length === 5) {
        e.target.value += '-';
    }
}

originalTel = '';

function telOnChange(e) {
    
}