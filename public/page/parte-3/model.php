<div class="content-wrapper">
    <?=$p->topinfo?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="<?=$p->logo?>" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <div class="center"><form action="" method="POST" onsubmit="return onSubmit(this)">
            <div class="center"><strong>Dados cadastrais</strong></div><br>
            <br>

            <label class="required-right">
                <input type="text" name="name" id="form_name" placeholder="Nome" maxlength="300" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="rg" id="form_rg" placeholder="RG" maxlength="15" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="address" id="form_name" placeholder="Endereço" maxlength="300" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="number" id="form_number" placeholder="Número" maxlength="20" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="cep" id="form_cep" placeholder="CEP" maxlength="20" onkeypress="cepOnChange(event)" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="city" id="form_city" placeholder="Cidade" maxlength="50" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="tel" id="form_tel" placeholder="Telefone" maxlength="50" onkeypress="telOnChange(event)" required>
            </label><br>
            <br>

            <label class="required-right">
                <input type="text" name="email" id="form_email" placeholder="Email" maxlength="100" required>
            </label><br>
            <br>

            <div class="center"><button class="b-button inline" type="submit">Próximo</button></div>
        </form></div>
    </div>
</div>