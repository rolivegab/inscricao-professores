<?
    require_once( realpath(__DIR__.'/../../Core/Template.php'));

    $T = new T(__DIR__, 'pt-br', 'Inscrição professores');
    $T->setResourceStyles(['default', 'logo']);

    // Parameters:
    $T->addResource('logo', '/image/logo.png');

    // Model:
    $T->loadModel(__DIR__);
    $T->render();
?>