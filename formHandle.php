<?
    session_start();
    
    require_once(__DIR__.'/lib.php');
    require_once(__DIR__.'/config.php');
    require_once(__DIR__.'/CDB.php');

    use formHandle\CDB;

    $db = new CDB();

    // Veririca se há uma requisição AJAX:
    $json = json_decode(file_get_contents('php://input', true));
    if(!empty($json))
    {
        if(isset($json->areaid)) {
            $areaid = $json->areaid;
            $courses = $db->get_courses_from_area($areaid);
            echo json_encode($courses);
            die();
        }
    }

    if (!isset($_POST['cpf'])) {
        header('location: formulario.php');
        die();
    }

    $cpf = $_POST['cpf'];

    // VALIDA CPF:
    if (!validaCPF($cpf)) {
        header('location: formulario.php?cpfcheck=false');
        die();
    } else {
        // SALVA O CPF NO SESSION:
        $_SESSION['cpf'] = $cpf;
    }

    // VERIFICA SE O CPF JÁ EXISTE NO BANCO DE DADOS:
    $sql = $db->checkCPF($cpf);
    if($sql) {
        header('location: cpfexiste.php?cpf='.$cpf);
        die();
    }

    // Pega as areas no banco:
    $areasql = $db->area();
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title = "Formulário de Inscrição"; 
        $css = ['css/default.css', 'css/formHandle.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php'); 
    ?>
    <script>
        let courses = <? echo json_encode($areasql); ?>;
        let areaState = 1;

        function requestCourses(areaid, callback) {

            let xmlhttp = new XMLHttpRequest();
            let file = 'formHandle.php';
            let header = {
                areaid: areaid
            }

            xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
                    if (callback) {
                        callback(xmlhttp.responseText);
                    }
                } else {
                    // Fazer barra de carregamento aqui.
                }
            };

            xmlhttp.onerror = function(e) {
                console.log(e.message);
                return false;
            };

            xmlhttp.open('POST', file, true);
            xmlhttp.setRequestHeader('Content-type', 'application/json');
            xmlhttp.withCredentials = true;
            xmlhttp.send(JSON.stringify(header));
        }

        function areaStateIncrement(element) {
            if (areaState === 1) {
                document.getElementById('divcourse2').hidden = false;
                document.getElementById('areadecrement').hidden = false;
                firstCategoryLoad(2);
            } else if (areaState === 2) {
                document.getElementById('divcourse3').hidden = false;
                element.hidden = true;
                firstCategoryLoad(3);
            } else {
                return;
            }

            areaState++;
        }

        function areaStateDecrement(element) {
            if (areaState === 2) {
                document.getElementById('divcourse2').hidden = true;
                document.getElementById('area2').innerHTML = '';
                element.hidden = true;
            } else if (areaState === 3) {
                document.getElementById('divcourse3').hidden = true;
                document.getElementById('areaincrement').hidden = false;
                document.getElementById('area3').innerHTML = '';
            } else {
                return;
            }

            areaState--;
        }

        function firstCategoryLoad(id) {
            let select = document.getElementById('area' + id);
            if (select) {
                let option = document.createElement('option');
                select.innerHTML = '<option value="default">Selecionar</option>';
                for(let i = 0; i < courses.length; ++i) {
                    let option = document.createElement('option');
                    option.value = courses[i].id;
                    option.innerText = courses[i].name;
                    select.appendChild(option);
                }
            }
        }

        window.onload = function(index) {
            firstCategoryLoad(1);
        }

        function areaOnChange(element, index) {
            let areaid = element.value;
            let select = document.getElementById('course' + index);
            let label = select.parentNode;
            if (select) {
                select.innerHTML = '<option value="default">Selecionar</option>';
            } else {
                return;
            }
            if(areaid == 'default') {
                label.hidden = true;
                return;
            }
            requestCourses(areaid, function(responseText) {
                let courses = JSON.parse(responseText);
                for(let i = 0; i < courses.length; ++i) {
                    let option = document.createElement('option');
                    option.value = courses[i].id;
                    option.innerText = courses[i].name;
                    select.appendChild(option);
                    label.hidden = false;
                }
            });
        }

        function onSubmit(form) {
            if(
                !form.area1 || form.area1.value === 'default' || form.course1.value === 'default' ||
                !form.area2 || form.area2.value === 'default' || form.course2.value === 'default' ||
                !form.area3 || form.area3.value === 'default' || form.course3.value === 'default'
            ) {
                return false;
            }

            return true;
        }
    </script>
</head>