<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title="Formulário de Inscrição";
        $captcha=true;
        $css=['css/default.css', 'css/formulario.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php');
    ?>
    <script>
        let cpfcheck = <? 
            if(isset($_GET['cpfcheck']) && $_GET['cpfcheck'] === "false") {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            } ?>;

        window.onload = function() {
            if(!cpfcheck) {
                document.getElementById('cpferror').innerText = 'CPF Inválido';
            }
        }
    </script>
</head>
<body class="content-wrapper">
    <? require_once('top_info.php'); ?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="images/logo_v2.png" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <form action="formHandle.php" method="POST">
            <label class="required-right">
                <input type="text" name="cpf" id="form_cpf" placeholder="CPF" maxlength="11" required>
                <span id="cpferror" class="error"></span>
            </label><br>
            <label class="b-text">
                <input type="checkbox" required>
                Declaro que li e estou de acordo com todos os critérios de seleção e diretrizes estabelecidos no <a href="edital.pdf" download="Edital - Cadastro de Reserva - Professor Conteudista Pós-Graduação.pdf">edital</a> 01/2018 NEAD-UNIFACEX.
            </label>
            <div class="center"><button class="b-button" type="submit">Próximo</button></div>
            <!--<div class="g-recaptcha" data-sitekey="6Le0ykcUAAAAAASc9weA3-jMjuKUqOjNU1ob8VLl"></div>-->
        </form>
    </div>
</body>
</html>