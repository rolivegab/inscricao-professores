<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title="Formulário de Inscrição";
        $captcha=true;
        $css=['css/default.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php');
    ?>
</head>
<body class="content-wrapper">
    <div class="content">
        <? include('top_info.php'); ?>
        <div class="center"><img class="b-image" src="images/logo_v2.png" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <div class="center"><span>Já foi realizada uma inscrição com CPF igual a <b><? echo $_GET['cpf'] ?></b>.<br><br>
        Se você acredita que isso foi um erro, favor entrar em contato com a nossa equipe.<br>Mais informações no topo da página. </span></div><br>
        <br><br>
        <form action="index.php">
            <button class="b-button">Voltar</button>
        </form>
    </div>
</body>
</html>