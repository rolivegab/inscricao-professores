CREATE TABLE IF NOT EXISTS area (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS course (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS area_course (
    areaid INTEGER REFERENCES area(id),
    courseid INTEGER REFERENCES course(id),
    PRIMARY KEY(courseid, areaid)
);

CREATE TABLE IF NOT EXISTS submission (
    cpf VARCHAR(11) PRIMARY KEY,
    role VARCHAR(100) NOT NULL,
    name VARCHAR(300) NOT NULL,
    rg VARCHAR(15) NOT NULL,
    address VARCHAR(300) NOT NULL,
    number VARCHAR(20) NOT NULL,
    cep VARCHAR(20) NOT NULL,
    city VARCHAR(50) NOT NULL,
    tel VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    school_level VARCHAR(100) NOT NULL,
    graduation_for VARCHAR(150) NOT NULL,
    pos_graduation_for VARCHAR(150),
    employedbefore VARCHAR(20) NOT NULL,
    teacherexperience VARCHAR(500) NOT NULL,
    eadexperience VARCHAR(500) NOT NULL,
    contentmanagerexperience VARCHAR(500) NOT NULL,
    otherexperiences VARCHAR(500) NOT NULL,
    lattesfile VARCHAR(200) NOT NULL,
    rgfilefront VARCHAR(200) NOT NULL,
    rgfileverse VARCHAR(200),
    cpffile VARCHAR(200) NOT NULL,
    electorfile VARCHAR(200) NOT NULL,
    residencefile VARCHAR(200) NOT NULL,
    graduationfilefront VARCHAR(200) NOT NULL,
    graduationfileverse VARCHAR(200) NOT NULL,
    posgraduationfilefront VARCHAR(200),
    posgraduationfileverse VARCHAR(200),
    otherfiles1 VARCHAR(200),
    otherfiles2 VARCHAR(200),
    otherfiles3 VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS submission_course (
    courseid INTEGER REFERENCES course(id),
    cpf VARCHAR(11) REFERENCES submission(cpf),
    PRIMARY KEY(courseid, cpf)
);