<?
    require_once(__DIR__.'/CDB.php');
    use course\CDB;
    $db = new CDB();
    // Mostra lista de usuários.
    if(isset($_GET['excludePositives'])) {
        $submissions = $db->null_courses_from_submission(['cpf', 'name', 'email']);
    } else  {
        $submissions = $db->submission(['cpf', 'name', 'email']);
    }
    ?>
    <table>
        <thead>
            <tr>
                <th>cpf</th>
                <th>nome</th>
                <th>email</th>
            </tr>
        </thead>
        <tbody>
        <?
            foreach($submissions as $row) {
                ?>
                    <tr>
                        <td><a href="course2.php?cpf=<?=$row->cpf?>"><?=$row->cpf?></a></td>
                        <td><?=$row->name?></td>
                        <td><?=$row->email?></td>
                    </tr>
                <?
            }
        ?>
        </tbody>
    </table>