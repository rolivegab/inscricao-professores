<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title="Formulário de Inscrição";
        $captcha=true;
        $css=['css/default.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php');
    ?>
</head>
<body class="content-wrapper">
    <? require_once('top_info.php'); ?>
    <div class="content">
        <div class="center"><img class="b-image" src="images/logo_v2.png" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <div class="center"><span>Formulário enviado com sucesso!</span></div>
    </div>
</body>
</html>