<?
    require_once(__DIR__.'/CDB.php');
    use finalSubmission\CDB;
    use course\CDB as courseCDB;
    require_once(__DIR__.'/Log.php');
    session_start();

    function checkFiles($files) {

        $errors = [];

        foreach($files as $file) {
            if(!isset($_FILES[$file]) || $_FILES[$file]['size'] > 2097152) {
                $errors[] = $file;
            }
        }

        return $errors;
    }

    function appendSESSIONIfExists($parameters, &$array) {
        foreach($parameters as $parameter) {
            if(isset($_SESSION[$parameter])) {
               $array[] = $parameter;
            }
        }
    }

    function appendFILEIfExists($parameters, &$array) {
        foreach($parameters as $parameter) {
            if(isset($_FILES[$parameter])) {
                $array[] = $parameter;
            }
        }
    }

    function checkParameters($parameters) {
        $errors = [];

        foreach($parameters as $parameter) {
            if (!isset($_SESSION[$parameter])) {
                $errors[] = $parameter;
            }
        }

        return $errors;
    }

    function saveFile($path, $file) {
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
        while(true) {
            $time = explode(' ', microtime());
            $filename = $path.$time[1].(round($time[0]*1000, 0)).'.'.$extension;
            if(file_exists($filename)) {
                usleep(1);
                continue;
            } else {
                if(move_uploaded_file($file['tmp_name'], $filename)) {
                    return $filename;
                } else {
                    return NULL;
                }
            }
        }
    }

    $log = new Log('finalFormHandle.txt');
    $log->putL('Usuário: '.$_SESSION['cpf']);
    $log->putL('Nome: '.$_SESSION['name']);

    // Checando sessão

    $sessionParameters = [
        'cpf',
        'role',
        'area1',
        'course1',
        'name',
        'rg',
        'address',
        'number',
        'cep',
        'city',
        'tel',
        'email',
        'school_level',
        'graduation_for',
        'employedbefore',
        'teacherexperience',
        'eadexperience',
        'contentmanagerexperience',
        'otherexperiences'
    ];
    
    $sessionPossiblyParameters = [
        'area2',
        'course2',
        'area3',
        'course3'
    ];

    appendSESSIONIfExists($sessionPossiblyParameters, $sessionParameters);
    $errors = checkParameters($sessionParameters);
    if(count($errors) > 0) {
        $log->putL('Erro nos parâmetros: ');
        foreach($errors as $error) {
            $log->put($error.', ');
        }
        $log->putL('');
        header('location: formError.php');
        die();
    } 

    // Checando arquivos:

    $files = [
        'lattesfile',
        'rgfilefront',
        'cpffile',
        'electorfile',
        'residencefile',
        'graduationfilefront',
        'graduationfileverse'
    ];

    $filePossiblyParameters = [
        'rgfileverse',
        'posgraduationfilefront',
        'posgraduationfileverse',
        'otherfiles',
        'otherfiles2',
        'otherfiles3'
    ];

    appendFILEIfExists($filePossiblyParameters, $files);
    $errors = checkFiles($files);
    if(count($errors) > 0) {
        $log->putL('Erro nos parâmetros: ');
        foreach($errors as $error) {
            $log->put($error.', ');
        }
        $log->putL('');
        header('location: formError.php');
        die();
    }

    // Salva os arquivos:
    $uploadpath = __DIR__.'/upload/';
    $lattesfile = saveFile($uploadpath, $_FILES['lattesfile']);
    $rgfilefront = saveFile($uploadpath, $_FILES['rgfilefront']);
    $rgfileverse;
    if(isset($_FILES['rgfileverse'])) {
        $rgfileverse = saveFile($uploadpath, $_FILES['rgfileverse']);
    }
    $cpffilename = saveFile($uploadpath, $_FILES['cpffile']);
    $electorfile = saveFile($uploadpath, $_FILES['electorfile']);
    $residencefilename = saveFile($uploadpath, $_FILES['residencefile']);
    $graduationfilefrontname = saveFile($uploadpath, $_FILES['graduationfilefront']);
    $graduationfileversename = saveFile($uploadpath, $_FILES['graduationfileverse']);
    $posgraduationfilefrontname;
    $posgraduationfileversename;
    $otherfiles1;
    $otherfiles2;
    $otherfiles3;
    if(isset($_FILES['posgraduationfilefront'])) {
        $posgraduationfilefrontname = saveFile($uploadpath, $_FILES['graduationfilefront']);
    }
    if(isset($_FILES['posgraduationfileverse'])) {
        $posgraduationfileversename = saveFile($uploadpath, $_FILES['graduationfileverse']);
    }
    if(isset($_FILES['otherfiles1'])) {
        $otherfiles1 = saveFile($uploadpath, $_FILES['otherfiles1']);
    }
    if(isset($_FILES['otherfiles2'])) {
        $otherfiles2 = saveFile($uploadpath, $_FILES['otherfiles2']);
    }
    if(isset($_FILES['otherfiles3'])) {
        $otherfiles3 = saveFile($uploadpath, $_FILES['otherfiles3']);
    }

    // Salva no banco:
    $db = new CDB();
    $cdb = new courseCDB();

    $params = [
        'cpf' => $_SESSION['cpf'],
        'role' => $_SESSION['role'],
        'name' => $_SESSION['name'],
        'rg' => $_SESSION['rg'],
        'address' => $_SESSION['address'],
        'number' => $_SESSION['number'],
        'cep' => $_SESSION['cep'],
        'city' => $_SESSION['city'],
        'tel' => $_SESSION['tel'],
        'email' => $_SESSION['email'],
        'school_level' => $_SESSION['school_level'],
        'graduation_for' => $_SESSION['graduation_for'],
        'pos_graduation_for' => $_SESSION['pos_graduation_for'],
        'employedbefore' => $_SESSION['employedbefore'],
        'teacherexperience' => $_SESSION['teacherexperience'],
        'eadexperience' => $_SESSION['eadexperience'],
        'contentmanagerexperience' => $_SESSION['contentmanagerexperience'],
        'otherexperiences' => $_SESSION['otherexperiences'],
        'lattesfile' => $lattesfile,
        'rgfilefront' => $rgfilefront,
        'rgfileverse' => isset($rgfileverse) ? $rgfileverse : 'NULL',
        'cpffile' => $cpffilename,
        'electorfile' => $electorfile,
        'residencefile' => $residencefilename,
        'graduationfilefront' => $graduationfilefrontname,
        'graduationfileverse' => $graduationfileversename,
        'posgraduationfilefront' => isset($posgraduationfilefrontname) ? $posgraduationfilefrontname : 'NULL',
        'posgraduationfileverse' => isset($posgraduationfileversename) ? $posgraduationfileversename : 'NULL',
        'otherfiles1' => isset($otherfiles1) ? $otherfiles1 : 'NULL',
        'otherfiles2' => isset($otherfiles2) ? $otherfiles2 : 'NULL',
        'otherfiles3' => isset($otherfiles3) ? $otherfiles3 : 'NULL'
    ];

    $result = $db->saveSubmission($params);

    // Acoplando cursos:
    $lastid = $cdb->add_course_to_submission($_SESSION['course1'], $_SESSION['cpf']);
    $log->putL($_SESSION['course1'].' '.$lastid);

    if(isset($_SESSION['course2'])) {
        $cdb->add_course_to_submission($_SESSION['course2'], $_SESSION['cpf']);
        $log->putL($_SESSION['course2'].' '.$lastid);
    }

    if(isset($_SESSION['course3'])) {
        $cdb->add_course_to_submission($_SESSION['course3'], $_SESSION['cpf']);
        $log->putL($_SESSION['course3'].' '.$lastid);
    }

    $log->close();

    include_once(__DIR__.'/success.php');
?>