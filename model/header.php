<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title><? echo isset($title) ? $title : "" ?></title>
<? echo isset($captcha) ? "<script src='https://www.google.com/recaptcha/api.js'></script>" : "" ?>
<? foreach ($css as $c) {
    echo '<link rel="stylesheet" href="'.$c.'">';
}?>