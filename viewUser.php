<?
    session_start();
    use admin\CDB;
    use course\CDB as courseCDB;
    require_once('CDB.php');

    if(!isset($_SESSION['logged']) || !isset($_GET['cpf'])) {
        include_once(__DIR__.'/notLogged.php');
        die();
    }

    $db = new CDB();
    $cdb = new courseCDB();
    $submission = $db->submission_with_cpf($_GET['cpf']);
    $courses = $cdb->courses_from_submission(['id', 'name'], $_GET['cpf']);
    if(!$submission) {
        die();
    }

    function filename($file) {
        return pathinfo($file, PATHINFO_FILENAME).'.pdf';
    }

    $lattesfile = filename($submission->lattesfile);
    $rgfilefront = filename($submission->rgfilefront);
    $rgfileverse = filename($submission->rgfileverse);
    $cpffile = filename($submission->cpffile);
    $electorfile = filename($submission->electorfile);
    $residencefile = filename($submission->residencefile);
    $graduationfilefront = filename($submission->graduationfilefront);
    $graduationfileverse = filename($submission->graduationfileverse);
    $posgraduationfilefront = filename($submission->posgraduationfilefront);
    $posgraduationfileverse = filename($submission->posgraduationfileverse);
    $otherfiles1 = filename($submission->otherfiles1);
    $otherfiles2 = filename($submission->otherfiles2);
    $otherfiles3 = filename($submission->otherfiles3);
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <? 
            $title="Formulário de Inscrição";
            $captcha=true;
            $css=['css/default.css', 'css/logo.css', 'css/font-awesome.css', 'css/property.css'];
            require_once( __DIR__.'/model/header.php' );
        ?>
        <script>

            function addToDomLoad(f) {
                window.addEventListener("DOMContentLoaded", f, false);
            }

            // parsing role:
            addToDomLoad(function() {
                let span = document.getElementById('role');
                let text = span.innerText;
                if(text === 'both') {
                    span.innerText = 'Ambos';
                } else if(text === 'teacher') {
                    span.innerText = 'Professor conteudista';
                } else if(text === 'reviewer') {
                    span.innerText = 'Revisor de conteúdo';
                }
            });

            window.onload = function() {
                let blocks = document.querySelectorAll('[data-type]');
                let checkboxes = document.getElementById('checkboxes');
                blocks.forEach(function(i) {
                    let label = document.createElement('label');
                    let checkbox = document.createElement('input');
                    checkbox.type = 'checkbox';
                    checkbox.onclick = showOrHide.bind(null, checkbox);
                    checkbox.checked = true;
                    label.appendChild(checkbox);
                    label.appendChild(document.createTextNode(i.getAttribute('data-type')));
                    checkboxes.appendChild(label);
                });
            }

            function showOrHide(element) {
                let text = element.parentNode.innerText;
                let block = document.querySelector('[data-type="' + text + '"]');
                if(!element.checked) {
                    block.hidden = true;
                } else {
                    block.hidden = false;
                }
            }

        </script>
    </head>

</html>