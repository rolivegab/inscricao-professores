<?
    session_start();

    if (
        !isset($_POST['name']) ||
        !isset($_POST['rg']) ||
        !isset($_POST['address']) ||
        !isset($_POST['number']) ||
        !isset($_POST['cep']) ||
        !isset($_POST['city']) ||
        !isset($_POST['tel']) ||
        !isset($_POST['email'])
    ) {
        var_dump($_POST);
        header('location: index.php');
        die();
    }

    $_SESSION['name'] = $_POST['name'];
    $_SESSION['rg'] = $_POST['rg'];
    $_SESSION['address'] = $_POST['address'];
    $_SESSION['number'] = $_POST['number'];
    $_SESSION['cep'] = $_POST['cep'];
    $_SESSION['city'] = $_POST['city'];
    $_SESSION['tel'] = $_POST['tel'];
    $_SESSION['email'] = $_POST['email'];
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <? 
        $title = "Formulário de Inscrição"; 
        $css = ['css/default.css', 'css/thirdFormHandle.css', 'css/logo.css', 'css/font-awesome.css'];
        require_once(__DIR__.'/model/header.php'); 
    ?>
    <script>
        function onSubmit(form) {
            if(
                !form ||
                !form.school_level ||
                !form.graduation_for
            ) {
                return false;
            }

            return true;
        }
    </script>
</head>
<body class="content-wrapper">
    <? require_once('top_info.php'); ?>
    <div class="content alert">
        <div class="center"><img class="b-image" src="images/logo_v2.png" alt="Logo da Sala Virtual da UNIFACEX"></div>
        <form action="fourthFormHandle.php" method="POST" onsubmit="return onSubmit(this)">
            <div class="center"><strong>Formação Acadêmica</strong></div><br>
            <br>
            <span class="question required">Maior nível:</span><br>
            <br>
            <label>
                <input type="radio" name="school_level" id="graduation" value="graduation" required>
                Graduação
            </label><br>
            <label>
                <input type="radio" name="school_level" id="graduation" value="specialization" required>
                Especialização
            </label><br>
            <label>
                <input type="radio" name="school_level" id="graduation" value="masters" required>
                Mestrado
            </label><br>
            <label>
                <input type="radio" name="school_level" id="graduation" value="doctorate" required>
                Doutorado
            </label><br>
            <br><br>

            <label class="question required">
                Graduação em:<br>
                <input type="text" name="graduation_for" id="graduation_for" required maxlength="150">
            </label><br>
            <br>
            
            <label class="question">
                Pós-Graduação em (maior nível, se houver):<br>
                <input type="text" name="pos_graduation_for" id="pos_graduation_for" maxlength="150">
            </label><br>
            <br>

            <div class="center"><button class="b-button" type="submit">Próximo</button></div>
        </form>
    </div>
</body>
</html>